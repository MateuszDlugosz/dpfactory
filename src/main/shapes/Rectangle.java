package main.shapes;

import main.utils.Position;

public class Rectangle extends Shape {

	private float width;
	private float height;
	
	public Rectangle() {
		super(new Position(0, 0));
		
		this.width = 10f;
		this.height = 20f;
	}
	
	public Rectangle(Position position, float width, float height) {
		super(position);
		
		this.width = width;
		this.height = height;
	}
	
	public float getWidth() {
		return width;
	}
	
	public float getHeight() {
		return height;
	}

	@Override
	public void scale(float scale) {
		width = width / scale;
		height = height / scale;
	}
	
	@Override
	public String toString() {
		return "Rectangle width:" + width + " " + "height:" + height + ".";
	}
	
}
