package main.shapes;

import main.utils.Position;

public abstract class Shape {

	private Position position;
	
	public Shape(Position position) {
		this.position = position;
	}
	
	public Position getPosition() {
		return position;
	}
	
	public abstract void scale(float scale);
	
}
