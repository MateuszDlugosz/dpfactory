package main.shapes;

import main.utils.Position;

public class Circle extends Shape {

	private float radius;
	
	public Circle() {
		super(new Position(0,0));
		
		this.radius = 10f;
	}
	
	public Circle(Position position, float radius) {
		super(position);
		
		this.radius = radius;
	}

	public float getRadius() {
		return radius;
	}

	@Override
	public void scale(float scale) {
		radius = radius / scale;
	}
	
	@Override
	public String toString() {
		return "Circle radius:" + radius + ".";
	}
	
}
