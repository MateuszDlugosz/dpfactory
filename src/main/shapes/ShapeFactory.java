package main.shapes;

public class ShapeFactory {

	public Shape getDefaultShape(ShapeType shapeType) {
		switch (shapeType) {
			case CIRCLE:
				return new Circle();
			case RECTANGLE:
				return new Rectangle();
			default:
				throw new IllegalArgumentException("Unknown shape type.");
		}
	}
	
}
