package main;

import java.util.ArrayList;
import java.util.List;

import main.shapes.Shape;
import main.shapes.ShapeFactory;
import main.shapes.ShapeType;

public class ShapeFactoryTest {

	public static void main(String[] args) {
		ShapeFactory factory = new ShapeFactory();
		List<Shape> shapes = new ArrayList<Shape>();
		
		float scale = 5;
		
		shapes.add(factory.getDefaultShape(ShapeType.CIRCLE));
		shapes.add(factory.getDefaultShape(ShapeType.CIRCLE));
		shapes.add(factory.getDefaultShape(ShapeType.RECTANGLE));
		
		System.out.println(shapes);
		
		for (Shape shape : shapes) {
			shape.scale(scale);
		}
		
		System.out.println(shapes);
	}
	
}
